package com.viking.bootstrap;

import com.sun.jna.platform.win32.Kernel32;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.mule.runtime.module.reboot.MuleContainerBootstrap;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

public class MuleContainerBootstrapWrapper {

    private static String key = "viking";

    public static void main(String[] args) throws Exception {
        int pid = Kernel32.INSTANCE.GetCurrentProcessId();
        try (PrintWriter out = new PrintWriter("D:/mule/mule-kernel-4.2.1-magento/pid.txt")) {
            out.println(pid);
        }
        List<String> argList = Arrays.stream(args).filter(arg -> arg.startsWith("props.location")).collect(Collectors.toList());
        Map<String, String> props = retrieveProperties(argList.get(0).substring(15));
        for (Map.Entry<String, String> entry : props.entrySet()) {
            setSystemProperty(entry.getKey(), entry.getValue());
        }
        MuleContainerBootstrap.main(args);
    }

    private static void setSystemProperty(String propKey, String propValue) {
        if (propValue.startsWith("![") && propValue.endsWith("]")) {
            StandardPBEStringEncryptor decryptor = new StandardPBEStringEncryptor();
            decryptor.setPassword(key);
            System.setProperty(propKey, decryptor.decrypt(propValue.substring(2, propValue.length() - 1)));
        } else {
            System.setProperty(propKey, propValue);
        }
    }

    private static Map<String, String> retrieveProperties(String filePath) {
        Map<String, String> propsMap = new HashMap<>();
        try (InputStream inputStream = new FileInputStream(filePath)) {
            Properties props = new Properties();
            props.load(inputStream);
            props.forEach((key, value) -> propsMap.put(key.toString(), value.toString()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return propsMap;
    }
}
